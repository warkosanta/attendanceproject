using AccountService.Helpers;
using AccountService.Interfaces;
using AccountService.Models;
using AttendanceProjectLibrary.Interfaces;
using EntitiesLibrary.Entities;
using EntitiesLibrary.Helpers;
using EntitiesLibrary.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace AccountService.Services
{
    public class UserService : IUserService
    {
        private IAppDataContext _context;
        private AppSettings _settings;

        public UserService(IAppDataContext context, AppSettings settings)
        {
            _context = context;
            _settings = settings;
        }

        /// <summary>
        /// Plain user authentication
        /// </summary>
        /// <returns></returns>
        public IUniversityMember Authenticate(AuthenticateModel authData)
        {
            var user = _context.Users.SingleOrDefault(u => u.Email == authData.Username);

            // check if username exists
            if (user == null)
                throw new AppException(message: "������������ �� ������.");

            // check if password is correct
            if (!PasswordHashHandler.VerifyPasswordHash(authData.Password, user.PasswordHash, user.PasswordSalt))
                throw new AppException(message: "������ ��������.");

            // check if mobile id is correct

            if (user.MobileID != authData.MobileID)
                throw new AppException(message: "������� ����� � ������ ����������.");

            // if user is both student and instructer, student takes priority
            var student = _context.Students.SingleOrDefault(s => s.User == user);
            if (student != null) return student;
            else
            {
                var instructor = _context.Instructors.SingleOrDefault(i => i.User == user);
                if (instructor != null) return instructor;
            }

            // user present in the system, but does not identify as a student or teacher
            throw new AppException(message: "������������ ���������� � �������," +
                " ������ �� �������� �� �������� �� ��������������.");
        }

        /// <summary>
        /// Create authentification token.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string TokenHandler(User user)
        {
            int expireDays = 7;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_settings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.ID.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(expireDays),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }

        // TODO:
        // Create method - for all users
        // Upadet methos - for mobile users, args+=mobileID
        public User Registrate(User user, RegisterModel model)
        {
            if (_context.Users.Any(x => x.Email == model.Email))
                throw new AppException($"Email: {model.Email} �����.");

            byte[] passwordHash, passwordSalt;
            PasswordHashHandler.CreatePasswordHash(model.Password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            if (model.UserType == MobileAppUser.Student)
            {
                if (string.IsNullOrEmpty(model.Group)) throw new AppException("��� ����������� �������� ���������� ������� ������.");
                if (!_context.Groups.Any(g => g.ID == model.Group)) throw new AppException("��������� ������ �� ����������.");

                var student = new Student { User = user, GroupID = model.Group };
                _context.Students.Add(student);
                _context.SaveChanges();
            }
            else if (model.UserType == MobileAppUser.Instructor)
            {
                var instructor = new Instructor { User = user };

                _context.Instructors.Add(instructor);
                _context.SaveChanges();
            }
            else
            {
                throw new AppException("���������� ������� ��� �������������� � ����������: " +
                    "������� ��� �������������.");
            }

            return user;
        }

        // TODO: this
        public void Update(User userParam, string password = null)
        {
            var user = _context.Users.SingleOrDefault(u => u.ID == userParam.ID);

            if (user == null)
                throw new AppException("������������ �� ������");

            // update username if it has changed
            if (!string.IsNullOrWhiteSpace(userParam.Email) && userParam.Email != user.Email)
            {
                // throw error if the new username is already taken
                if (_context.Users.Any(x => x.Email == userParam.Email))
                    throw new AppException("Username " + userParam.Email + " is already taken");

                user.Email = userParam.Email;
            }

            // update user properties if provided
            if (!string.IsNullOrWhiteSpace(userParam.FirstName))
                user.FirstName = userParam.FirstName;

            if (!string.IsNullOrWhiteSpace(userParam.LastName))
                user.LastName = userParam.LastName;

            if (!string.IsNullOrWhiteSpace(userParam.MobileID))
                user.MobileID = userParam.MobileID;

            // update password if provided
            if (!string.IsNullOrWhiteSpace(password))
            {
                byte[] passwordHash, passwordSalt;
                PasswordHashHandler.CreatePasswordHash(password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            _context.Users.Update(user);
            _context.SaveChanges();
        }
        public User GetById(string id)
        {
            return _context.Users.Single(user => user.ID == Guid.Parse(id));
        }
    }
}