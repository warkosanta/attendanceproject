﻿using AccountService.Models;
using AttendanceProjectLibrary.Interfaces;
using EntitiesLibrary.Entities;

namespace AccountService.Interfaces
{
    /// <summary>
    /// Manage users.
    /// </summary>
    public interface IUserService
    { /// <summary>
      /// Authenticate user.
      /// </summary>
        IUniversityMember Authenticate(AuthenticateModel authenticateModel);

        /// <summary>
        /// Registrate user.
        /// </summary>
        User Registrate(User user, RegisterModel registerModel);

        /// <summary>
        /// Update user info.
        /// </summary>
        void Update(User user, string password = null);

        /// <summary>
        /// Get user by it's ID. Used at startup class.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        User GetById(string id); 
    }
}
