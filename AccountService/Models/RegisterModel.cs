using System.ComponentModel.DataAnnotations;

namespace AccountService.Models
{
    /// <summary>
    /// ��������������, ��� ��������� ����������� ����� ������������ �������� � �������������.
    /// </summary>
    public enum MobileAppUser { Student, Instructor }

    /// <summary>
    /// ������ ��� �����������.
    /// </summary>
    public class RegisterModel
    {
        /// <summary>
        /// ��� ������������.
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// ������� ������������.
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// �������� ������������.
        /// </summary>
        /// <remarks>
        /// ���� �� ��������������� ������ ���������� ��������.
        /// </remarks>
        [Required]
        public string Patronymic { get; set; }

        /// <summary>
        /// ������ ��.�����.
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// ������.
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// ������������� � ���������� ������������ ������������ ��� ������� ��� ��� �������������.
        /// �� ����� ����� �������� � ����� �� ������ �� ����� ��������� ��� ������.
        /// </summary>
        [Required]
        public MobileAppUser UserType { get; set; }

        /// <summary>
        /// ���������� ������������� ���������� ����������.
        /// </summary>
        /// <remarks>
        /// �������� ����������� ����� ��� ����� �������������� ��������������.
        /// ���� �� ��������������� ������ ����������� �� � ���������� ����������.
        /// </remarks>
        [Required]
        public string MobileID { get; set; }

        /// <summary>
        /// ���� ������������ �������������� ��� �������, ������ �������� ����������� �����.
        /// </summary>
        public string Group { get; set; }
    }
}