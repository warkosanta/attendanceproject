namespace AccountService.Models
{
    /// <summary>
    /// ������ ��� ��������� ������ ������������.
    /// </summary>
    public class UpdateModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string MobileID { get; set; }
    }
}