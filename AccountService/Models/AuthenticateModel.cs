﻿using System.ComponentModel.DataAnnotations;

namespace AccountService.Models
{
    /// <summary>
    /// Данные для аутентификации пользователя.
    /// </summary>
    public class AuthenticateModel
    {
        /// <summary>
        /// Email пользователя.
        /// </summary>
        [Required]
        public string Username { get; set; }

        /// <summary>
        /// Пароль пользователя.
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// ID мобильного устройства пользователя.
        /// </summary>
        [Required]
        public string MobileID { get; set; }
    }
}