﻿using System;
using AccountService.Interfaces;
using AccountService.Models;
using AccountService.Services;
using AttendanceProjectLibrary.Interfaces;
using AutoMapper;
using EntitiesLibrary.Entities;
using EntitiesLibrary.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AttendanceProjectAPI.Controllers
{
    /// <summary>
    /// Manage users.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;
        private ILogger _logger;

        public AccountController(IUserService userService, IMapper mapper, ILogger<AccountController> logger)
        {
            _userService = userService;
            _mapper = mapper;
            _logger = logger;
        }

        /// <summary>
        /// Authenticate user.
        /// </summary>
        /// <remarks>
        /// Аутентификация пользователя, см. AuthenticateModel.
        /// </remarks>
        /// <param name="model">Данные для аутентификация формируются по модели AuthenticateModel.</param>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public  IActionResult Authenticate(AuthenticateModel model)
        {
            _logger.LogInformation("Trying to authenticate user with params: username: {}, mobile id: {}",
                model.Username, model.MobileID);

            IUniversityMember user;

            try
            {
                user = _userService.Authenticate(model);
            }
            catch (AppException ex)
            {
                _logger.LogWarning("Authentication for user {} failed with error: {}." +
                                                  " Message: {}", model.Username, ex.GetType(), ex.Message);

                return BadRequest(new {message = ex.Message});
            }
            catch(Exception ex)
            {
                _logger.LogError("Unhandled exception in authentication : {}, message: {}", ex.GetType(), ex.Message);
                return BadRequest();
            }

            var tokenString = (_userService as UserService)?.TokenHandler(user.User);

            _logger.LogInformation("User successfully authenticated: username: {}",
                model.Username);

            return Ok(new
            {
                user.User.ID,
                user.User.FirstName,
                user.User.LastName,
                Token = tokenString,
                group = user is Student ? (user as Student).Group.ID : ""
            }); 

        }

        /// <summary>
        /// Register new user.
        /// </summary>
        /// <remarks>
        /// Регистрация пользователя, см. RegisterModel.
        /// </remarks>
        /// <param name="model">Данные для регистрации формируются по модели RegisterModel.</param>
        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register(RegisterModel model)
        {
            _logger.LogInformation("Trying to register user with params: username: {}, type: {}",
                model.Email, model.UserType);

            var user = _mapper.Map<User>(model);

            try
            {
                _userService.Registrate(user, model);
                _logger.LogInformation("User successfully register: username: {}",
                    model.Email);
                return Ok();
            }
            catch (AppException ex)
            {
                _logger.LogWarning("Authentication for user {} failed with error: {}." +
                                   " Message: {}", model.Email, ex.GetType(), ex.Message);
                return BadRequest(new { message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError("Unhandled exception in registration : {}, message: {}", ex.GetType(), ex.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// Update user's data.
        /// </summary>
        /// <param name="id">User guid ID.</param>
        /// <param name="model">New data.</param>
        /// <remarks>
        /// Обновление данных о пользователе. id - Guid, см. UpdateModel.
        /// </remarks>
        [HttpPut("{id}")]
        public IActionResult Update(string id, UpdateModel model)
        {
            var user = _mapper.Map<User>(model);
            user.ID = Guid.Parse(id);

            try
            {
                _userService.Update(user, model.Password);
                _logger.LogInformation("User data successfully updated user id: {}", id);
                return Ok();
            }
            catch (AppException ex)
            {
                _logger.LogWarning("Updating for user id {} failed with error: {}." +
                                   " Message: {}", id, ex.GetType(), ex.Message);
                return BadRequest(new { message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError("Unhandled exception in update : {}, message: {}", ex.GetType(), ex.Message);
                return BadRequest();
            }
        }      
    }
}
