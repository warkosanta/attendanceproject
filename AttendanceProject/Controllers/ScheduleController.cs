﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ScheduleService.Models;

namespace AttendanceProjectAPI.Controllers
{
    /// <summary>
    /// Get schedule items.
    /// </summary>
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {
        private ScheduleService.Services.ScheduleService _scheduleService;
        private IMapper _mapper;
        public ScheduleController(ScheduleService.Services.ScheduleService schedule, IMapper autoMapper)
        {
            _scheduleService = schedule;
            _mapper = autoMapper;
        }

        /// <summary>
        /// Returns set of schedule items matching the given options.
        /// </summary>
        /// <remarks>
        /// Получить расписание. В случае, если парметры не указаны, будет возвращено все расписание из бд.
        /// В идеале, передать в параметрах группу, в этом случае будет возвращено расписание для четных и нечетных недель.
        /// Обратить внимание: занятия английского и физики, как пример, имеют в расписании несколько преподавателей и несколько аудиторий,
        /// в этом случае метод вернет по каждому элементу на кабинет и приподавателя.
        /// Например, пара иностранного языка будет состоять из 4 элементов. 
        /// Необходимо просто соединить их в одно: соединить преподавателей и аудитории, в остальном эти 4 элемента будут идентичны.
        /// </remarks>
        /// <param name="scheduleParams">Параметры поиска формировать по модели ScheduleParams.</param>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<ScheduleDTO> Get([FromQuery]ScheduleParams scheduleParams)
        {
            List<ScheduleDTO> schedules = new List<ScheduleDTO>();  
            _mapper.Map(_scheduleService.GetScheduleItem(scheduleParams), schedules);
            return schedules;
        }
    }
}
