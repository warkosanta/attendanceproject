﻿using AttendanceService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentService.Models;
using StudentService.Services;

namespace AttendanceProjectAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class AttendanceController : ControllerBase
    {
        private AttendanceManager _attendanceManager;
        public AttendanceController(AttendanceManager studentManager)
        {
            _attendanceManager = studentManager;
        }
        // TODO: params model
        [HttpPost]
        public IActionResult Post(AttendModel attendModel)
        {
            attendModel.UserID = HttpContext.User.Identity.Name;
            if (ModelState.IsValid)
            {
                _attendanceManager.Attend(attendModel);
                return Ok();
            }
            else
            {
                return BadRequest(ModelState.ValidationState);
            }
        }

        [HttpGet]
        public IActionResult Get(AttendanceGetParams getParams)
        {
            if (ModelState.IsValid)
            {
                return Ok(_attendanceManager.GetAttendedStudents(getParams));
            }
            else
            {
                return BadRequest(ModelState.ValidationState);
            }
        }
    }
}
