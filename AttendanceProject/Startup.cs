﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System;
using Microsoft.OpenApi.Models;
using AccountService.Interfaces;
using AccountService.Services;
using EntitiesLibrary.Helpers;
using WebApi.Helpers;
using EntitiesLibrary.Interfaces;
using StudentService.Services;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;
using ScheduleParser;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace WebApi
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _configuration;
        private readonly ILoggerFactory _loggerFactory;

        public Startup(IWebHostEnvironment env, IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _env = env;
            _configuration = configuration;
            _loggerFactory = loggerFactory;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Attendance Project API" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme."
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                            new string[] {}

                    }
                });

                //get the full location of the assembly with DaoTests in it
                string fullPath = Assembly.GetAssembly(typeof(ScheduleService.Services.ScheduleService)).Location;

                //get the folder that's in
                string theDirectory = Path.GetDirectoryName(fullPath);


                var xmlFiles = Directory.GetFiles(theDirectory, "*.xml", SearchOption.AllDirectories);
                foreach (var file in xmlFiles)
                {
                    c.IncludeXmlComments(file);
                }
            });

            services.AddControllers();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            // configure strongly typed settings objects
            var appSettingsSection = _configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        var userService = context.HttpContext.RequestServices.GetRequiredService<IUserService>();
                        var userId = context.Principal.Identity.Name;
                        var user = userService.GetById(userId);
                        if (user == null)
                        {
                            // return unauthorized if user no longer exists
                            context.Fail("Unauthorized");
                        }
                        return Task.CompletedTask;
                    }
                };
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            services.AddMvc(option => option.EnableEndpointRouting = false)
                .AddNewtonsoftJson(opt => opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);

            // configure DI for application services
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAppDataContext, DataContext>();
            services.AddTransient<AttendanceManager>();
            services.AddTransient<ScheduleService.Services.ScheduleService>();
            services.AddSingleton<AppSettings>(appSettings);
            services.AddTransient<IScheduleParser, ScheduleParser.ScheduleParser>();
            services.AddTransient<IStuffParser, StuffParser>();

            // for migrations
            services.AddDbContext<DataContext>(option =>
                option.UseSqlServer(connectionString: _configuration.GetConnectionString("LocalConnection"), x => x.UseNetTopologySuite()));

            // for application, same usage in unit tests
            services.AddTransient<IDataProvider, DataContext>(provider =>
            {
                var options = new DbContextOptionsBuilder<DataContext>()
                                            .UseSqlServer(_configuration.GetConnectionString("RemoteConnection"),
                                            x => x.UseNetTopologySuite())
                                            .UseLazyLoadingProxies()
                                            .Options;
                var newContext = new DataContext(options, _loggerFactory.CreateLogger<DataContext>());
                // apply new migrations and run
                //newContext.Database.Migrate();
                return newContext;
            });
            services.AddTransient<IAppDataContext, DataContext>(provider =>
            {
                var options = new DbContextOptionsBuilder<DataContext>()
                                            .UseSqlServer(_configuration.GetConnectionString("RemoteConnection"),
                                            x => x.UseNetTopologySuite())
                                            .UseLazyLoadingProxies()
                                            .Options;

                var newContext = new DataContext(options, _loggerFactory.CreateLogger<DataContext>());
                // apply new migrations and run
                //newContext.Database.Migrate();
                return newContext;
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Заполнение пустой бд первичными тестовыми данными.
            //using (var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            //{
            //    var context = scope.ServiceProvider.GetService<DataContext>();
            //    context.EnsureDatabaseSeeded();
            //}

            #region parser tests
            var db = (IAppDataContext)app.ApplicationServices.GetService(typeof(IAppDataContext));
            var parser = (IScheduleParser)app.ApplicationServices.GetService(typeof(IScheduleParser));


            var stuffParser = (IStuffParser)app.ApplicationServices.GetService(typeof(IStuffParser));
            var results = stuffParser.GetStuffList();

            //var res = parser.GetScheduleByGroup("КИ19-12Б");
            //db.Schedule.AddRange(res);

            //res = parser.GetScheduleByGroup("КИ18-13Б");
            //db.Schedule.AddRange(res);

            //res = parser.GetScheduleByGroup("КИ19-11Б");
            //db.Schedule.AddRange(res);

            //res = parser.GetScheduleByGroup("ЭУ 19-05 МЭФ модуль 4 (с 24.04 -30.05)");
            //db.Schedule.AddRange(res);

            //db.SaveChanges();
            #endregion

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Attendance Project API V1");
            });

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}
