using AccountService.Models;
using AttendanceService.Models;
using AutoMapper;
using EntitiesLibrary.Entities;
using ScheduleService.Models;
using StudentService.Models;
using System;
using DayOfWeek = EntitiesLibrary.Entities.DayOfWeek;

namespace WebApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<RegisterModel, User>();
            CreateMap<UpdateModel, User>();
            CreateMap<AttendModel, Attendance>();
            CreateMap<Student, StudentDTO>()
                .ForMember(desp => desp.LastName, opt => opt.MapFrom(src => src.User.LastName))
                .ForMember(desp => desp.FirstName, opt => opt.MapFrom(src => src.User.FirstName));
                
            CreateMap<Schedule, ScheduleDTO>()
                .ForMember(desp => desp.DayOfWeek, opt => opt.MapFrom(src => Enum.GetName(typeof(DayOfWeek), src.DayOfWeek)))
                .ForMember(desp => desp.WeekType, opt => opt.MapFrom(src => Enum.GetName(typeof(WeekType), src.WeekType)))
                .ForMember(desp => desp.ClassType, opt => opt.MapFrom(src => Enum.GetName(typeof(ClassType), src.ClassType)))
                .ForMember(desp => desp.InstructorName, opt => opt.MapFrom(src => src.Instructor.User.LastName + ' ' + src.Instructor.User.FirstName + ' ' +  src.Instructor.User.Patronymic ))
                .ForMember(desp => desp.Group, opt => opt.MapFrom(src => src.GroupID));
        }
    }
}