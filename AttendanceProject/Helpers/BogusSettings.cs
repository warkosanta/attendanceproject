﻿using System.Collections.Generic;
using System.Linq;
using Bogus;
using EntitiesLibrary.Entities;
using EntitiesLibrary.Interfaces;

namespace AttendanceProjectAPI.Helpers
{
    /// <summary>
    /// Найстройки генерации новых экземпляров с помощью Bogus.
    /// </summary>
    static public class BogusSettings
    {
        static private Faker<User> usesrFaker = new Faker<User>("ru")
            .RuleFor(o => o.Email, f => f.Internet.Email())
            .RuleFor(o => o.FirstName, f => f.Name.FirstName())
            .RuleFor(o => o.LastName, f => f.Name.LastName());

        static private Faker<Subject> subjectsFaker = new Faker<Subject>("ru")
            .RuleFor(o => o.Description, f => f.Lorem.Sentence(3));

        static private Faker<Audience> audienceFaker = new Faker<Audience>("ru")
            .RuleFor(o => o.Number, f => f.Random.Number(1000).ToString())
            .RuleFor(o => o.Corpus, f => f.Lorem.Letter(1));


        public static List<User> CreateTestUsers(int amount)
        {
            var users = usesrFaker.Generate(amount);
            return users;
        }

        public static List<Subject> CreateTestSubjects(int amount)
        {
            var newSubjects = subjectsFaker.Generate(amount);
            return newSubjects;
        }

        public static List<Audience> CreateTestAudience(int amount)
        {
            var newAudiences = audienceFaker.Generate(amount);
            return newAudiences;
        }

        public static List<Attendance> CreateTestAttendanceItems(IAppDataContext context, int amount)
        {
            Faker<Attendance> AttendanceItemsFaker = new Faker<Attendance>()
            .RuleFor(o => o.Schedule, f => f.PickRandom<Schedule>(context.Schedule))
            .RuleFor(o => o.Student, f => f.PickRandom<Student>(context.Students))
            .RuleFor(o => o.StudentID, f => f.PickRandom<int>(context.Students.Select(s => s.ID)))
            .RuleFor(o => o.DateTime, f => f.Date.Past());

            return AttendanceItemsFaker.Generate(amount);
        }
   
        public static List<Instructor> CreateTestInstructors(IAppDataContext context)
        {
            Faker<Instructor> instructorsFaker = new Faker<Instructor>()
            .RuleFor(o => o.Speciality, f => f.Lorem.Sentence(3))
            .RuleFor(o => o.User, f => f.PickRandom<User>(context.Users));

            return instructorsFaker.Generate(context.Users.Count() - 2);
        }

        public static List<Student> CreateTestStudents(IAppDataContext context)
        {
            Faker<Student> studentsFaker = new Faker<Student>()
            .RuleFor(o => o.Group, f => f.PickRandom<Group>(context.Groups))
            .RuleFor(o => o.User, f => f.PickRandom<User>(context.Users));

            return studentsFaker.Generate((context.Users.Count() - 2));
        }

        public static List<Schedule> CreateTestScheduleItems(IAppDataContext context, int amount)
        {
            Faker<Schedule> ScheduleItemsFaker = new Faker<Schedule>()
            .RuleFor(o => o.AudienceID, f => f.PickRandom<int>(context.Audiences.Select(a => a.ID)))
            .RuleFor(o => o.DayOfWeek, f => f.PickRandom<DayOfWeek>())
            .RuleFor(o => o.Group, f => f.PickRandom<Group>(context.Groups))
            .RuleFor(o => o.Instructor, f => f.PickRandom<Instructor>(context.Instructors))
            .RuleFor(o => o.Subject, f => f.PickRandom<Subject>(context.Subjects))
            .RuleFor(o => o.Term, f => f.PickRandom<TermTime>(context.Terms))
            .RuleFor(o => o.WeekType, f => f.PickRandom<WeekType>());

            return ScheduleItemsFaker.Generate(amount);
        }
    }
}