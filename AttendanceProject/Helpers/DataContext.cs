using EntitiesLibrary.Entities;
using EntitiesLibrary.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace WebApi.Helpers
{
    public class DataContext : DbContext, IAppDataContext
    {
        protected readonly IConfiguration Configuration;
        private readonly ILogger<DataContext> _logger;

        public DataContext() { }

        public DataContext(DbContextOptions<DataContext> options, ILogger<DataContext> logger = null)
            : base(options)
        {
            _logger = logger;
        }

        // people
        public DbSet<User> Users { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<Student> Students { get; set; }

        // schedule
        public DbSet<Schedule> Schedule { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<TermTime> Terms { get; set; }
        public DbSet<Attendance> Attendance { get; set; }

        // objects
        public DbSet<Audience> Audiences { get; set; }
        public DbSet<Subject> Subjects { get; set; }


        public int GetInstructorIDByName(List<string> name, GetInstructorSettings settings)
        {
            Instructor instructor = null;

            switch (settings)
            {
                case GetInstructorSettings.FullName:
                    instructor = Instructors.SingleOrDefault(i => i.User.LastName == name[0] && i.User.FirstName == name[1] && i.User.Patronymic == name[2]);
                    break;
                case GetInstructorSettings.SurnameAndInitials:
                    instructor = Instructors.SingleOrDefault(i => i.User.LastName == name[0] && i.User.Initials == $"{name[1]}. {name[2]}.");
                    break;
            }
            
            if (instructor is null)
            {
                instructor = new Instructor
                {
                    User = new User
                    {
                        LastName = name[0],
                        FirstName = name[1],
                        Initials = $"{name[1][0]}. {name[2][0]}.",
                        Patronymic = name[2],
                        Role = Roles.Instructor
                    }
                };
                Instructors.Add(instructor);
                SaveChanges();

                _logger?.LogInformation($"New instructor was created: {instructor.User.LastName} {instructor.User.FirstName} {instructor.User.Patronymic}");
            }

            return instructor.ID;
        }

        public Instructor GetInstructorByID(int id)
        {
            return Instructors.Find(id);
        }

        public int GetAudienceID(string corpus, string number)
        {
            var audience = Audiences.SingleOrDefault(a => a.Corpus == corpus && a.Number == number);
            if (audience is null)
            {
                audience = new Audience { Corpus = corpus, Number = number };
                Audiences.Add(audience);
                SaveChanges();

                _logger?.LogInformation($"New audience item was created: corpus \"{audience.Corpus}\" number \"{audience.Number}\"");
            }

            return audience.ID;
        }

        public int GetSubjectID(string subjectName)
        {
            var subject = Subjects.SingleOrDefault(s => s.Name == subjectName);
            if (subject is null)
            {
                subject = new Subject { Name = subjectName };
                Subjects.Add(subject);
                SaveChanges();
            }

            return subject.ID;
        }

        public void InsertNewGroup(string groupName)
        {
            var group = Groups.SingleOrDefault(g => g.ID == groupName);
            if (group is null)
            {
                Groups.Add(new Group { ID = groupName });
                SaveChanges();
            }
        }
    }

}