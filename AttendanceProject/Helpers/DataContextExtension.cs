﻿using EntitiesLibrary.Entities;
using System;
using System.Linq;
using WebApi.Helpers;

namespace AttendanceProjectAPI.Helpers
{
    /// <summary>
    /// Метод расширения для класса контекста данных.
    /// Используется для создания тестовых данных при создании базы данных.
    /// </summary>
    public static class DataContextExtension
    {
        /// <summary>
        /// Задать первичные тестовые данные для пустой базы данных.
        /// </summary>
        public static void EnsureDatabaseSeeded(this DataContext context)
        {
            // constant data

            if (!context.Terms.Any())
            {
                var newTerms = new TermTime[]
                {
                    new TermTime(){ StartsAt = new TimeSpan(8,30,0).ToString(@"hh\:mm"),  EndsAt = new TimeSpan(10,5,00).ToString(@"hh\:mm")},
                    new TermTime(){ StartsAt = new TimeSpan(10,15,0).ToString(@"hh\:mm"), EndsAt = new TimeSpan(11,50,00).ToString(@"hh\:mm")},
                    new TermTime(){ StartsAt = new TimeSpan(12,00,0).ToString(@"hh\:mm"), EndsAt = new TimeSpan(13,35,00).ToString(@"hh\:mm")},
                    new TermTime(){ StartsAt = new TimeSpan(14,10,0).ToString(@"hh\:mm"), EndsAt = new TimeSpan(15,45,00).ToString(@"hh\:mm")},
                    new TermTime(){ StartsAt = new TimeSpan(15,55,0).ToString(@"hh\:mm"), EndsAt = new TimeSpan(17,30,00).ToString(@"hh\:mm")},
                    new TermTime(){ StartsAt = new TimeSpan(17,40,0).ToString(@"hh\:mm"), EndsAt = new TimeSpan(19,15,00).ToString(@"hh\:mm")},
                    new TermTime(){ StartsAt = new TimeSpan(19,25,0).ToString(@"hh\:mm"), EndsAt = new TimeSpan(21,00,00).ToString(@"hh\:mm")},
                };
                context.Terms.AddRange(newTerms);
                context.SaveChanges();
            }

            if (!context.Groups.Any())
            {
                var newGroups = new Group[]
                {
                    new Group(){ID = "КИ18-11Б", Course = 1},
                    new Group(){ID = "КИ18-12Б", Course = 1},
                    new Group(){ID = "КИ18-13Б", Course = 1},
                    new Group(){ID = "КИ18-14Б", Course = 1},
                    new Group(){ID = "КИ18-18Б", Course = 1},
                };
                context.Groups.AddRange(newGroups);
                context.SaveChanges();
            }

            if (!context.Subjects.Any())
            {
                var newSubjects = BogusSettings.CreateTestSubjects(10);
                context.Subjects.AddRange(newSubjects);
                context.SaveChanges();
            }

            if (!context.Audiences.Any())
            {
                var newAudiences = BogusSettings.CreateTestAudience(5);
                context.Audiences.AddRange(newAudiences);
                context.SaveChanges();
            }

            // people

            if (!context.Users.Any())
            {
                var newUsers = BogusSettings.CreateTestUsers(10);
                context.Users.AddRange(newUsers);
                context.SaveChanges();
            }

            if (!context.Students.Any())
            {
                var newStudents = BogusSettings.CreateTestStudents(context);
                context.Students.AddRange(newStudents);
                context.SaveChanges();
            }

            if (!context.Instructors.Any())
            {
                var newInstructors = BogusSettings.CreateTestInstructors(context);
                context.Instructors.AddRange(newInstructors);
                context.SaveChanges();
            }

            // objects

            if (!context.Schedule.Any())
            {
                var newScheduleItems = BogusSettings.CreateTestScheduleItems(context, 10);
                context.Schedule.AddRange(newScheduleItems);
                context.SaveChanges();
            }

            if (!context.Attendance.Any())
            {
                var newAttendanceItems = BogusSettings.CreateTestAttendanceItems(context, 15);
                context.Attendance.AddRange(newAttendanceItems);
                context.SaveChanges();
            }
        }
    }
}