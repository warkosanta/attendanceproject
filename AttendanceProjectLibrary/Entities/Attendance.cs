﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EntitiesLibrary.Entities
{
    public class Attendance
    {
        [Key]
        public int ID { get; private set; }

        public Guid ScheduleID { get; set; }

        public virtual Schedule Schedule { get; set; }

        public int StudentID { get; set; }

        public virtual Student Student { get; set; }

        public DateTime DateTime { get; set; }
    }
}
