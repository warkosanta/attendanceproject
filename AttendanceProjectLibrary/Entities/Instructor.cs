﻿using System.ComponentModel.DataAnnotations;

namespace EntitiesLibrary.Entities
{
    public class Instructor: AttendanceProjectLibrary.Interfaces.IUniversityMember
    {
        [Key]
        public int ID { get; set; }

        public System.Guid UserID { get; set; }

        public virtual User User { get; set; }

        public string Speciality { get; set; }
    }
}
