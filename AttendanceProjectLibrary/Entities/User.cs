using NetTopologySuite.Geometries;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EntitiesLibrary.Entities
{
    public enum Roles { Student, Instructor } // etc

    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Patronymic { get; set; }

        public string Email { get; set; }

        public Roles Role { get; set; }

        /// <summary>
        /// Students location. SRID = 4326.
        /// </summary>
        public Point Location { get; set; }

        public string MobileID { get; set; }

        [MaxLength(5)]
        public string Initials { get; set; }

        [JsonIgnore]
        public byte[] PasswordHash { get; set; }

        [JsonIgnore]
        public byte[] PasswordSalt { get; set; }
    }
}