﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EntitiesLibrary.Entities
{
    public class TermTime
    {
        /// <summary>
        /// Class number 1,2,3...7.
        /// </summary>
        [Key]
        public int ClassNumber { get; set; }

        private TimeSpan startsAt;

        public string StartsAt {
            get
            {
                return startsAt.ToString();
            }

            set
            {
                startsAt = TimeSpan.Parse(value);
            }
        }

        private TimeSpan endsAt;

        public string EndsAt
        {
            get
            {
                return endsAt.ToString();
            }

            set
            {
                endsAt = TimeSpan.Parse(value);
            }
        }
    }
}
