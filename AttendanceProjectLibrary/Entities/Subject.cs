﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntitiesLibrary.Entities
{
    public class Subject
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }
    }
}
