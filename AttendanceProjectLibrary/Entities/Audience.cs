﻿using NetTopologySuite.Geometries;
using System.ComponentModel.DataAnnotations;

namespace EntitiesLibrary.Entities
{
    public class Audience
    {
        [Key]
        public int ID { get; set; }

        public string Number { get; set; }

        public string Corpus { get; set; }

        // Database includes both Polygon and MultiPolygon values
        public Geometry Border { get; set; }
    }
}
