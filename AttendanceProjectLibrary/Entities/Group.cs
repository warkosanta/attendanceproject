﻿using System.ComponentModel.DataAnnotations;

namespace EntitiesLibrary.Entities
{
    /// <summary>
    /// Student group.
    /// </summary>
    public class Group
    {
        /// <summary>
        /// Typically has a simple pattern like "КИ18-13Б",
        /// but sometimes could have more complicated pattern.
        /// </summary>
        [Key]
        public string ID { get; set; }

        /// <summary>
        /// Group course in range 1 to 6.
        /// </summary>
        [Range(1, 6)]
        public int Course { get; set; }
    }
}
