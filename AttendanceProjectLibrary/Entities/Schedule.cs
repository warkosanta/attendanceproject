﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntitiesLibrary.Entities
{
    /// <summary>
    /// Week type.
    /// </summary>
    public enum WeekType
    {
        odd,
        even
    }

    /// <summary>
    /// Day of week.
    /// </summary>
    public enum DayOfWeek { monday = 1, tuesday = 2, wednesday = 3, thursday = 4, friday = 5, saturday = 6, sunday = 7 }

    /// <summary>
    /// Class type.
    /// </summary>
    public enum ClassType { Practice, Lecture, Laboratory, Other }

    /// <summary>
    /// Schedule item.
    /// </summary>
    public class Schedule : ICloneable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public System.Guid ID { get; set; }

        public WeekType WeekType { get; set; }

        public DayOfWeek DayOfWeek { get; set; }

        public ClassType ClassType { get; set; }

        public int TermID { get; set; }

        public virtual TermTime Term { get; set; }

        public string GroupID { get; set; }

        public virtual Group Group { get; set; }

        public int? InstructorID { get; set; }

        public virtual Instructor Instructor { get; set; }

        public int? AudienceID { get; set; }

        public virtual Audience Audience { get; set; }

        public int? SubjectID { get; set; }

        public virtual Subject Subject { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
