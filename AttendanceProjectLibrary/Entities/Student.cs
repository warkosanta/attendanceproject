﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EntitiesLibrary.Entities
{
    public enum StudentRoles { Ordinary, Praepostor } // etc

    public class Student: AttendanceProjectLibrary.Interfaces.IUniversityMember
    {
        [Key]
        public int ID { get; set; }

        public Guid UserID { get; set; }

        public virtual User User { get; set; }

        public string GroupID { get; set; }

        public virtual Group Group { get; set; }

        public StudentRoles Role { get; set; }
    }
}
