﻿using EntitiesLibrary.Entities;

namespace AttendanceProjectLibrary.Interfaces
{
    public interface IUniversityMember
    {
        public User User { get; set; }

    }
}
