﻿using System;
using System.Collections.Generic;
using System.Text;
using EntitiesLibrary.Entities;

namespace EntitiesLibrary.Interfaces
{
    public interface IScheduleParser
    {
        public List<Schedule> GetFullSchedule();
        public List<Schedule> GetScheduleByGroup(string groupName);
    }
}
