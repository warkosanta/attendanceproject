﻿using EntitiesLibrary.Entities;
using Microsoft.EntityFrameworkCore;

namespace EntitiesLibrary.Interfaces
{
    public interface IAppDataContext : IDataProvider
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Schedule> Schedule { get; set; }
        public DbSet<Attendance> Attendance { get; set; }
        public DbSet<Audience> Audiences { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<TermTime> Terms { get; set; }
        public int SaveChanges(); // + SaveChangesAsync
    }
}
