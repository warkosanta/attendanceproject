﻿using System;
using System.Collections.Generic;
using System.Text;
using EntitiesLibrary.Entities;

namespace EntitiesLibrary.Interfaces
{
    public interface IStuffParser
    {
        public List<Instructor> GetStuffList();
    }
}
