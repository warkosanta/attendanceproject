﻿using System;
using System.Collections.Generic;
using System.Text;
using EntitiesLibrary.Entities;

namespace EntitiesLibrary.Interfaces
{
    /// <summary>
    /// Настройки для параметра Name.
    /// </summary>
    public enum GetInstructorSettings
    {
        /// <summary>
        /// Параметр 'Name' - массив вида [Фамилия,Имя,Отчество]".
        /// </summary>
        FullName,

        /// <summary>
        /// Параметр 'Name' - массив вида [Фамилия,Первая буква имени,Первая буква отчества]".
        /// </summary>
        SurnameAndInitials,
    }

    public interface IDataProvider
    {
        public int GetInstructorIDByName(List<string> name, GetInstructorSettings settings);

        public Instructor GetInstructorByID(int id);

        public int GetAudienceID(string corpus, string number);

        public int GetSubjectID(string subjectName);

        /// <summary>
        /// Добавляют группу в базу данных, если таких ещё нет.
        /// </summary>
        public void InsertNewGroup(string groupName);
    }
}
