﻿using EntitiesLibrary.Entities;
using EntitiesLibrary.Interfaces;
using ScheduleService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using DayOfWeek = EntitiesLibrary.Entities.DayOfWeek;

namespace ScheduleService.Services
{
    public class ScheduleService
    {
        private IAppDataContext _context;

        public ScheduleService(IAppDataContext context)
        {
            _context = context;
        }

        public List<Schedule> GetScheduleItem(ScheduleParams scheduleParams)
        {
            IEnumerable<Schedule> schedule = _context.Schedule;
            if (!string.IsNullOrEmpty(scheduleParams.Group))
            {
                schedule = schedule.Where(s => s.GroupID.Contains(scheduleParams.Group.ToUpper()));
            }
            if (!string.IsNullOrEmpty(scheduleParams.WeekType))
            {
                if (Enum.IsDefined(typeof(WeekType), scheduleParams.WeekType))
                {
                    schedule = schedule.Where(s => Enum.GetName(typeof(WeekType), s.WeekType) == scheduleParams.WeekType);
                }
            }
            if (!string.IsNullOrEmpty(scheduleParams.DayOfWeek))
            {
                if (int.TryParse(scheduleParams.DayOfWeek, out int num))
                {
                    var RES = Enum.IsDefined(typeof(DayOfWeek), num);
                    if (RES) { schedule = schedule.Where(s => (int)s.DayOfWeek == num); };
                }
                else
                {
                    var RES = Enum.IsDefined(typeof(DayOfWeek), scheduleParams.DayOfWeek.ToLower());
                    if (RES) { schedule = schedule.Where(s => s.DayOfWeek.ToString() == scheduleParams.DayOfWeek.ToLower()); };
                }
            }
            if (!string.IsNullOrEmpty(scheduleParams.Instructor))
            {
                schedule = schedule.Where(s => scheduleParams.Instructor == $"{s.Instructor.User.FirstName} {s.Instructor.User.Patronymic} {s.Instructor.User.LastName}"
                                            || scheduleParams.Instructor == $"{s.Instructor.User.FirstName} {s.Instructor.User.LastName}"
                                            || scheduleParams.Instructor == $"{s.Instructor.User.Initials} {s.Instructor.User.LastName}");
            }
            return schedule.ToList();
        }
    }
}