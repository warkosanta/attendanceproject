﻿namespace ScheduleService.Models
{
    /// <summary>
    /// Sorting parameters for schedule.
    /// </summary>
    public class ScheduleParams
    {
        /// <summary>
        /// Group id supposed to be like "КИ18-13Б".
        /// </summary>
        public string Group { get; set; }

        /// <summary>
        /// Type of week. Supposed to be "odd" or "even".
        /// </summary>
        public string WeekType { get; set; }

        /// <summary>
        /// Day of week. Should be like "monday", "sunday" etc OR like numbers 1 to 7.
        /// </summary>
        public string DayOfWeek { get; set; }

        /// <summary>
        /// Instructor name. Could be last OR first name OR both.
        /// </summary>
        public string Instructor { get; set; }
    }
}
