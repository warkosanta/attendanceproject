﻿using EntitiesLibrary.Entities;

namespace ScheduleService.Models
{
    public class ScheduleDTO
    {
        public System.Guid ID { get; set; }
        public string Group { get; set; }
        public string WeekType { get; set; }
        public string DayOfWeek { get; set; }
        public TermTime Term { get; set; }
        public string InstructorName { get; set; }
        public string ClassType { get; set; }
        public Audience Audience { get; set; }
        public Subject Subject { get; set; }
    }
}
