﻿namespace ScheduleService.Models
{
    public class TermDTO
    {
        public string StartsAt { get; set; }
        public string EndsAt { get; set; }
    }
}
