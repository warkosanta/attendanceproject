# Разработка системы для учета посещаемости студентов


### Стек технологий:
* ASP.NET Core

* Entity Framework Core


### Также были использованы:
* [Swagger](https://swagger.io/)

* [Bogus](https://github.com/bchavez/Bogus)

* [AutoMapper](https://automapper.org/)

* xUnit, Moq

### Управление задачами в [trello](https://trello.com/b/jQQJbJHb/attendance-project)
### API доступно по адресу https://sibattendance.azurewebsites.net/swagger/index.html
### Технические спецификации находятся в [этом репозитории](https://gitlab.com/warkosanta/technical-specifications/-/tree/develop)