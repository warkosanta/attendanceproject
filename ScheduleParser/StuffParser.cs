﻿using System.Collections.Generic;
using System.Linq;
using System;
using AngleSharp;
using AngleSharp.Dom;
using EntitiesLibrary.Entities;
using EntitiesLibrary.Interfaces;
using ScheduleParser.Helpers;
using Microsoft.Extensions.Logging;

namespace ScheduleParser
{
    public class StuffParser : IStuffParser
    {
        private const string _baseUrl = "http://structure.sfu-kras.ru/people";
        private static readonly List<string> _teacherMarks = new List<string>()
        {
            "старший преподаватель",
            "доцент",
            "профессор",
            "ассистент",
            "преподаватель",
        };

        private readonly HttpLoader _httpLoader;
        private IDataProvider _dataProvider;
        private ILogger<StuffParser> _logger;

        public StuffParser(IDataProvider dataProvider, ILogger<StuffParser> logger)
        {
            _httpLoader = new HttpLoader();
            _dataProvider = dataProvider;
            _logger = logger;
        }

        public List<Instructor> GetStuffList()
        {
            var stuffList = new List<Instructor>();

            for (int page = 0; page < 60; page++)
            {
                try
                {
                    IDocument document = PageLoader.GetPageDocument(_httpLoader.GetPageSourceAsync($"{_baseUrl}?page={page}").Result);

                    var people = document.QuerySelector("#people > tbody")
                        .GetElementsByTagName("tr");

                    /* 
                     * element.ChildNodes[0] - ФИО сотрудника
                     * element.ChildNodes[1] - вся информация о сотруднике, включая где он работает, должность, мобильные телефон и email
                    */

                    foreach (var element in people)
                    {
                        if (_teacherMarks.Any(mark => element.ChildNodes[1].TextContent.Contains(mark)))
                        {
                            List<string> splitedName = element.ChildNodes[0].TextContent.Split(' ', StringSplitOptions.RemoveEmptyEntries).ToList();
                            if (splitedName.Count == 2)
                                splitedName.Add("0");

                            var instructorID = _dataProvider.GetInstructorIDByName(splitedName, GetInstructorSettings.FullName);
                            stuffList.Add(_dataProvider.GetInstructorByID(instructorID));
                        }
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError($"Unhandled exception on parsing page {page}: {e.ToString()}");
                }
            }
            return stuffList;
        }
    }
}
