﻿using System.Collections.Generic;
using System.Linq;
using System;
using AngleSharp;
using AngleSharp.Dom;
using EntitiesLibrary.Entities;
using EntitiesLibrary.Interfaces;
using ScheduleParser.Helpers;
using DayOfWeek = EntitiesLibrary.Entities.DayOfWeek;
using Microsoft.Extensions.Logging;

namespace ScheduleParser
{
    public class ScheduleParser : IScheduleParser
    {
        private const string _baseUrl = "http://edu.sfu-kras.ru/timetable";

        private static readonly Dictionary<string, ClassType> _classTypesName = new Dictionary<string, ClassType>()
        {
            { "(практика)", ClassType.Practice },
            { "(лекция)", ClassType.Lecture },
            { "(лабораторная работа)", ClassType.Laboratory },
        };
        private static readonly Dictionary<string, DayOfWeek> _daysOfWeekInRussian = new Dictionary<string, DayOfWeek>()
        {
            { "Понедельник", DayOfWeek.monday },
            { "Вторник", DayOfWeek.tuesday },
            { "Среда", DayOfWeek.wednesday },
            { "Четверг", DayOfWeek.thursday },
            { "Пятница", DayOfWeek.friday },
            { "Суббота", DayOfWeek.saturday },
            { "Воскресенье", DayOfWeek.sunday }
        };
        private static readonly List<TermTime> _terms = new List<TermTime>()
        {
            new TermTime() {ClassNumber = 1, StartsAt = "8:30", EndsAt = "10:05"},
            new TermTime() {ClassNumber = 2, StartsAt = "10:15", EndsAt = "11:50"},
            new TermTime() {ClassNumber = 3, StartsAt = "12:00", EndsAt = "13:35"},
            new TermTime() {ClassNumber = 4, StartsAt = "14:10", EndsAt = "15:45"},
            new TermTime() {ClassNumber = 5, StartsAt = "15:55", EndsAt = "17:30"},
            new TermTime() {ClassNumber = 6, StartsAt = "17:40", EndsAt = "19:15"},
            new TermTime() {ClassNumber = 7, StartsAt = "19:25", EndsAt = "21:00"}
        };

        private readonly HttpLoader _httpLoader;
        private readonly ILogger<ScheduleParser> _logger;
        private IDataProvider _dataProvider;

        public ScheduleParser(IDataProvider dataProvider, ILogger<ScheduleParser> logger)
        {
            _httpLoader = new HttpLoader();
            _dataProvider = dataProvider;
            _logger = logger;
        }

        public List<Schedule> GetFullSchedule()
        {
            var allGroups = GetAllGroupsList();
            var fullSchedule = new List<Schedule>();

            foreach (var group in allGroups)
            {
                var result = GetScheduleByGroup(group);
                if (result != null) fullSchedule.AddRange(GetScheduleByGroup(group));
            }
            return fullSchedule;
        }

        public List<Schedule> GetScheduleByGroup(string group)
        {
            IDocument document = PageLoader.GetPageDocument(_httpLoader.GetPageSourceAsync(GetCorrectUrl($"{_baseUrl}?group={group}")).Result);
            try
            {
                var days = document.QuerySelectorAll("table")
                    .First(i => i.ClassName == "table timetable")
                    .QuerySelector("tbody")
                    .GetElementsByTagName("tr")
                    .Split(i => i.ClassName == "heading heading-section");

                var scheduleList = new List<Schedule>();
                foreach (var day in days)
                    scheduleList.AddRange(GetDaySchedule(day.ToList(), group));

                _logger.LogInformation($"Succesfully parsed group \"{group}\" with {scheduleList.Count} items");

                return scheduleList;
            }
            catch (Exception e)
            {
                _logger.LogError($"Unhandled exception on parsing group \"{group}\": {e.ToString()}");
                return new List<Schedule>();
            }
        }

        private string GetCorrectUrl(string url) =>
            new Uri(url).AbsoluteUri;

        private List<string> GetAllGroupsList()
        {
            IDocument document = PageLoader.GetPageDocument(_httpLoader.GetPageSourceAsync($"{_baseUrl}").Result);

            var institutes = document.QuerySelector("#node-3985 > div > section > ul")
                .GetElementsByTagName("li");

            var groupsList = new List<string>();

            foreach (var institute in institutes)
            {
                var courses = institute.QuerySelectorAll("ul")
                    .SelectMany(ul => ul.GetElementsByTagName("li"));

                foreach (var course in courses)
                {
                    var groups = course.QuerySelectorAll("ul")
                    .SelectMany(ul => ul.GetElementsByTagName("li"))
                    .Select(e => e.TextContent);

                    groupsList.AddRange(groups);
                }
            }

            return groupsList;
        }

        private List<Schedule> GetDaySchedule(List<IElement> dayElements, string group)
        {
            var daySchedule = new List<Schedule>();
            DayOfWeek day = _daysOfWeekInRussian[dayElements[0].TextContent];

            // taking nodes except <headings>
            foreach (IElement line in dayElements.TakeLast(dayElements.Count - 2))
            {
                IHtmlCollection<IElement> elements = line.Children;

                TermTime term = _terms.Find(t => t.ClassNumber == int.Parse(elements[0].TextContent));

                // first 2 elements for class number & term, others for class info
                // if first class info element has not any info, the second will contain anyway
                if (elements[2].HasChildNodes)
                {
                    daySchedule.AddRange(GetClass(elements[2], day, term, WeekType.odd, group));


                    // if only 1 element for class info, then need duplicate class for even week
                    if (elements.Length == 3)
                        daySchedule.AddRange(GetClass(elements[2], day, term, WeekType.even, group));
                    else
                        daySchedule.AddRange(GetClass(elements[3], day, term, WeekType.even, group));
                }
                else
                    daySchedule.AddRange(GetClass(elements[3], day, term, WeekType.even, group));
            }

            return daySchedule;
        }

        
        private List<Schedule> GetClass(IElement element, DayOfWeek dayOfWeek, TermTime termTime, WeekType weekType, string group)
        {

            var schedules = new List<Schedule>();

            var schedule = new Schedule()
            {
                WeekType = weekType,
                DayOfWeek = dayOfWeek,
                TermID = termTime.ClassNumber,
                GroupID = group
            };

            _dataProvider.InsertNewGroup(group);
            SetScheduleItemClassType(element, ref schedule);
            SetScheduleItemSubject(element, ref schedule);
            SetScheduleItemAudience(element, ref schedule, ref schedules);
            SetScheduleItemInstructor(element, ref schedule, ref schedules);

            if (schedule.AudienceID is null && schedule.InstructorID is null && schedule.SubjectID != null) return new List<Schedule> { schedule };

            return schedules;
        }

        private void SetScheduleItemSubject(IElement element, ref Schedule scheduleItem)
        {
            try
            {
                var subjectName = element.ChildNodes[0].TextContent;
                var subjectID = _dataProvider.GetSubjectID(subjectName);
                scheduleItem.SubjectID = subjectID;
            }
            catch (ArgumentOutOfRangeException)
            {
                scheduleItem.SubjectID = default;
            }
        }
        private void SetScheduleItemClassType(IElement element, ref Schedule scheduleItem)
        {
            try
            {
                scheduleItem.ClassType = _classTypesName[element.ChildNodes[1].TextContent.Trim()];
            }
            catch (ArgumentOutOfRangeException)
            {
                scheduleItem.ClassType = ClassType.Other;
            }
            catch (KeyNotFoundException)
            {
                scheduleItem.ClassType = ClassType.Other;
            }
        }
        private void SetScheduleItemAudience(IElement element, ref Schedule scheduleItem, ref List<Schedule> schedules)
        {
            string[] prefix = new string[] {    "пр./", "лаб./", "лек./", 
                                                "пр/", "лаб/", "лек/",
                                                "пр.,", "лаб.,", "лек.," };

            try
            {
                string fullAudience;
                switch (element.ChildNodes.Length)
                {
                    case 6:
                        fullAudience = element.ChildNodes[5].TextContent;
                        break;
                    case 5:
                        fullAudience = element.ChildNodes[4].TextContent;
                        break;
                    default: throw new ArgumentOutOfRangeException();
                }

                // prefix deliting;
                foreach (var p in prefix)
                    fullAudience = fullAudience.Replace(p, string.Empty);
                fullAudience.Trim();

                // removing shit in hooks, cause it always dates
                if (fullAudience.IndexOf('(') != -1)
                    fullAudience = fullAudience.Remove(fullAudience.IndexOf('('));

                // in cases, where corpus and numbers separating by '.'
                fullAudience = fullAudience.Replace('.', ' ');

                var corpus = fullAudience.Split(' ', StringSplitOptions.RemoveEmptyEntries).First();

                fullAudience = fullAudience.Replace(corpus, string.Empty);
                fullAudience.Trim();

                string[] numbers = fullAudience.Split(',').Select(s => s.Trim())
                                            .Distinct().ToArray();

                foreach (var number in numbers)
                {
                    if (number.Any(c => Char.IsLetter(c)))
                        continue;

                    var audienceID = _dataProvider.GetAudienceID(corpus, number);
                    scheduleItem.AudienceID = audienceID;

                    schedules.Add((Schedule)scheduleItem.Clone());
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                scheduleItem.AudienceID = default;
            }
        }

        private void SetScheduleItemInstructor(IElement element, ref Schedule scheduleItem, ref List<Schedule> schedules)
        {
            try
            {
                IHtmlCollection<IElement> nodeElements = element.GetElementsByTagName("em");

                if (nodeElements == null || nodeElements.Length == 0)
                    throw new ArgumentOutOfRangeException();

                string[] instructorFullName = nodeElements[0].TextContent.Split(",");

                var newScheduleList = new List<Schedule>();
                foreach (var name in instructorFullName)
                {
                    List<string> nameSplit = name.Trim().Replace(".", string.Empty).Split(' ', StringSplitOptions.RemoveEmptyEntries).ToList();
                    if (nameSplit.Count == 2)
                        nameSplit.Add("0");

                    var instructorID = _dataProvider.GetInstructorIDByName(nameSplit, GetInstructorSettings.SurnameAndInitials);

                    foreach (var item in schedules)
                    {
                        item.InstructorID = instructorID;
                        newScheduleList.Add((Schedule)item.Clone());
                    }
                }
                schedules = newScheduleList;
            }
            catch (ArgumentOutOfRangeException)
            {
                scheduleItem.InstructorID = default;
            }
        }       
    }
}