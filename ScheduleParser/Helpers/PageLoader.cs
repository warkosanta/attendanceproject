﻿using System;
using System.Collections.Generic;
using System.Text;
using AngleSharp;
using AngleSharp.Dom;

namespace ScheduleParser.Helpers
{
    internal static class PageLoader
    {
        private static readonly IConfiguration _config = Configuration.Default;
        private static readonly IBrowsingContext _context = BrowsingContext.New(_config);

        public static IDocument GetPageDocument(string pageSource)
        {
            return _context.OpenAsync(req => req.Content(pageSource)).Result;
        }
    }
}
