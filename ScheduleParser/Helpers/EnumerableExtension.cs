﻿using System;
using System.Collections.Generic;

namespace ScheduleParser.Helpers
{
    public static class EnumerableExtension
    {
        public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> list, Func<T, bool> predicate)
        {
            List<T> lastList = null;
            List<List<T>> fulllist = new List<List<T>>();
            foreach (var item in list)
            {
                if (predicate.Invoke(item))
                {
                    if (lastList != null)
                        fulllist.Add(lastList);
                    lastList = new List<T> { item };
                }
                else
                {
                    lastList.Add(item);
                }
            }
            fulllist.Add(lastList);
            return fulllist;
        }
    }
}
