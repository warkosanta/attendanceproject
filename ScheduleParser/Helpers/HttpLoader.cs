﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;

namespace ScheduleParser.Helpers
{
    internal class HttpLoader
    {
        public async Task<string> GetPageSourceAsync(string url)
        {
            string source = null;

            var resultGot = false;
            while (!resultGot)
            {
                try
                {
                    var httpClient = new HttpClient();
                    HttpResponseMessage response = await httpClient.GetAsync(url);
                    
                    if (response != null && response.StatusCode == HttpStatusCode.OK)
                    {
                        source = await response.Content.ReadAsStringAsync();
                    }

                    resultGot = true;
                }
                finally { }
            }

            return source;
        }
    }
}
