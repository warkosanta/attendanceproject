﻿using EntitiesLibrary.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using WebApi.Helpers;
using Xunit;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace AttendanceTests
{
    /// <summary>
    /// Тест на соответствие результатам парсинга реальному расписанию.
    /// Каждый предмет с двумя преподавателями/предметами умножается на два.
    /// Предмет одинаковый в четном и нечетном рассписании также умножает количество записей на два.
    /// Пример: Физика - 2 преподавателя, 2 аудитории => 4 записи в расписании заместо 1.
    /// </summary>
    public class ScheduleParserTests
    {
        /// <summary>
        /// Общее количество записей в расписании группы.
        /// </summary>
        [Theory]
        [InlineData("КИ18-13Б", 43)]
        [InlineData("КИ18-18Б", 47)]
        [InlineData("ЭУ 19-05 МЭФ модуль 4 (с 24.04 -30.05)", 29)]
        [InlineData("ИИ18-05Б, ИИ18-05БА, ИИ18-05БА1, ИИ18-05БА2 (подгруппа 1)", 36)] 
        public void ScheduleParser_GetScheduleByGroup_ShouldReturnCorrectTotalCount(string group, int count)
        {
            var result = ReturnCount(group: group);
            Assert.Equal(count, result);
        }

        /// <summary>
        /// Количество записей в четном расписании.
        /// </summary>
        [Theory]
        [InlineData("КИ18-13Б", 21)]
        [InlineData("КИ18-18Б", 24)]
        [InlineData("ЭУ 19-05 МЭФ модуль 4 (с 24.04 -30.05)", 14)]
        [InlineData("ИИ18-05Б, ИИ18-05БА, ИИ18-05БА1, ИИ18-05БА2 (подгруппа 1)", 18)]
        public void ScheduleParser_GetScheduleByGroup_ShouldReturnCorrectEvenCount(string group, int count)
        {

            var result = ReturnCount(group: group, 
                                 predicate: (s) => s.WeekType == WeekType.even);

            Assert.Equal(count, result);
        }

        /// <summary>
        /// Количество записей в нечетном расписании.
        /// </summary>
        [Theory]
        [InlineData("КИ18-13Б", 22)]
        [InlineData("КИ18-18Б", 23)]
        [InlineData("ЭУ 19-05 МЭФ модуль 4 (с 24.04 -30.05)", 15)]
        [InlineData("ИИ18-05Б, ИИ18-05БА, ИИ18-05БА1, ИИ18-05БА2 (подгруппа 1)", 18)]
        public void ScheduleParser_GetScheduleByGroup_ShouldReturnCorrectOddCount(string group, int count)
        {
            var result = ReturnCount(group: group,
                             predicate: (s) => s.WeekType == WeekType.odd);

            Assert.Equal(count, result);
        }

        private int ReturnCount(string group, Func<Schedule, bool> predicate = null)
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                 .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                 .Options;

            var logger = LoggerFactory.Create(b => b.ClearProviders()).CreateLogger<ScheduleParser.ScheduleParser>();

            using (var context = new DataContext(options))
            {
                var parser = new ScheduleParser.ScheduleParser(context, logger);
                if (predicate != null)
                {
                    return parser.GetScheduleByGroup(group).Where(predicate).Count();
                }
                return parser.GetScheduleByGroup(group).Count;
            }
        }
    }
}