﻿using EntitiesLibrary.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using WebApi.Helpers;
using Xunit;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace AttendanceTests
{
    public class StuffTestsContext : IDisposable
    {
        public List<string> StuffList;
        private DataContext _context;

        public StuffTestsContext()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                 .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                 .Options;

            _context = new DataContext(options);

            var logger = LoggerFactory.Create(b => b.ClearProviders()).CreateLogger<ScheduleParser.StuffParser>();

            var parsedList = new ScheduleParser.StuffParser(_context, logger).GetStuffList();
            StuffList = parsedList.Select(i => $"{i.User.LastName} {i.User.FirstName} {i.User.Patronymic}").ToList();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }

    /// <summary>
    /// Тест на проверку парсинга всех преподавателей и исключение остального состава сотрудников.
    /// </summary>
    public class StuffParserTests : IClassFixture<StuffTestsContext>
    {
        StuffTestsContext _fixture;

        public StuffParserTests(StuffTestsContext fixture)
        {
            _fixture = fixture;
        }

        /// <summary>
        /// Наличие преподавателя в распарсеном списке.
        /// </summary>
        [Theory]
        [InlineData("Виденин Сергей Александрович")]
        [InlineData("Кушнаренко Елена Евгеньевна")]
        [InlineData("Смирнов Евгений Сергеевич")]
        //[InlineData("Кушнаренко Андрей Викторович")]
        [InlineData("Кириллова Светлана Владимировна")]
        [InlineData("Корец Анатолий Яковлевич")]
        [InlineData("Вайнштейн Юлия Владимировна")]
        [InlineData("Даниленко Алексей Сергеевич")]
        [InlineData("Романовская Анна Александровна")]
        [InlineData("Лобасова Марина Спартаковна")]
        public void StuffParser_GetStuffList_ShouldContainInlinedata(string employeeName)
        {
            Assert.Contains(employeeName, _fixture.StuffList);
        }

        /// <summary>
        /// Отсутствие не преподавателя в распарсеном списке
        /// </summary>
        [Theory]
        [InlineData("Кузнецов Матвей Николаевич")]
        [InlineData("Абовян Анжела Айказовна")]
        [InlineData("Беляев Сергей Владимирович")]
        [InlineData("Беляева Елена Николаевна")]
        [InlineData("Гайнанова Татьяна Ивановна")]
        [InlineData("Дмитриева Валентина Борисовна")]
        [InlineData("Ибе Алексей Александрович")]
        public void StuffParser_GetStuffList_ShouldNotContainInlinedata(string employeeName)
        {
            Assert.DoesNotContain(employeeName, _fixture.StuffList);
        }
    }
}
