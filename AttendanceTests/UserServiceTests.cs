﻿using AccountService.Helpers;
using AccountService.Models;
using AccountService.Services;
using AttendanceProjectLibrary.Interfaces;
using EntitiesLibrary.Entities;
using EntitiesLibrary.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using WebApi.Helpers;
using Xunit;

namespace AttendanceTests
{
    public class UserServiceTests
    {
        /// <summary>
        /// Проверка успешного прохождения регистрации.
        /// </summary>
        /// <param name="user">Дополнительные данные пользователя.</param>
        /// <param name="registerModel">Данные для регистрации. Данные должны быть правильными.</param>
        /// <param name="groups">Список групп для хранения в базе данных.</param>
        [Theory]
        [ClassData(typeof(RegisterTestValidData))]
        public void UserService_Registrate_ShouldNotThrowException(User user, RegisterModel registerModel, Group[] groups)
        {
            try
            {
                var userService = SetUpUserService(out var _, groups);
                userService.Registrate(user, registerModel);
            }
            catch
            {
                Assert.False(true);
            }
        }

        /// <summary>
        /// Проверка неуспешного прохождения регистрации.
        /// </summary>
        /// <param name="user">Дополнительные данные пользователя.</param>
        /// <param name="registerModel">Данные для регистрации. Содержат ошибки.</param>
        /// <param name="groups">Список групп для хранения в базе данных.</param>
        [Theory]
        [ClassData(typeof(RegisterTestInvalidData))]
        public void UserService_Registrate_ShouldThrowAppException(User user, RegisterModel registerModel, Group[] groups)
        {
            Assert.Throws<AppException>(() =>
            {
                var userService = SetUpUserService(out var _, groups);
                userService.Registrate(user, registerModel);

            });
        }

        /// <summary>
        /// Проверка успешного прохождения аутентификации.
        /// </summary>
        /// <param name="authModel">Модель с данными аутентификации.</param>
        /// <param name="user">Пользователь в базе данных. Совпадает с моделью аутентификации.</param>
        [Theory]
        [ClassData(typeof(AuthenticateTestValidData))]
        public void UserService_Authenticate_ShouldNotThrowException(AuthenticateModel authModel, IUniversityMember user)
        {
            try
            {
                var userService = SetUpUserService(out var _, user: user);
                userService.Authenticate(authModel);
            }
            catch
            {
                Assert.False(true);
            }
        }

        /// <summary>
        /// Проверка неудачной аутентификации.
        /// </summary>
        /// <param name="authModel">Модель с данными аутентификации.</param>
        /// <param name="user">Пользователь в базе данных. Не совпадает с моделью аутентификации.</param>
        [Theory]
        [ClassData(typeof(AuthenticateTestInvalidData))]
        public void UserService_Authenticate_ShoulThrowAppException(AuthenticateModel authModel, IUniversityMember user)
        {
            Assert.Throws<AppException>(() =>
            {
                var userService = SetUpUserService(out var _, user: user);
                userService.Authenticate(authModel);
            });
        }

        /// <summary>
        /// Проверка успешного изменения данных пользователя.
        /// </summary>
        /// <param name="oldUser">Пользователь уже существующий в бд.</param>
        /// <param name="updatedUser">Обновленные данные пользователя.</param>
        /// <param name="password">Новый пароль.</param>
        [Theory]
        [ClassData(typeof(UpdateTestValidData))]
        public void UserService_Update_ShouldNotThrowException(User oldUser, User updatedUser, string password = null)
        {
            try
            {
                var userID = Guid.Empty;
                var userService = SetUpUserService(out userID, oldUser: oldUser);

                updatedUser.ID = userID;

                userService.Update(updatedUser, password);

                Assert.Equal(updatedUser.Email, userService.GetById(userID.ToString()).Email);
            }
            catch
            {
                Assert.False(true);
            }
        }

        /// <summary>
        /// Проверка неудачного изменения данных пользователя.
        /// </summary>
        /// <param name="oldUser">Пользователь уже существующий в бд.</param>
        /// <param name="updatedUser">Обновленные данные пользователя.</param>
        /// <param name="password">Новый пароль.</param>
        [Theory]
        [ClassData(typeof(UpdateTestInvalidData))]
        public void UserService_Update_ShouldThrowAppException(User oldUser, User updatedUser, string password = null)
        {
            Assert.Throws<AppException>(() =>
            {
                var userService = SetUpUserService(out var _, oldUser: oldUser);
                userService.Update(updatedUser, password);
            });
        }

        private UserService SetUpUserService(out Guid userID, Group[] groups = null, IUniversityMember user = null, User oldUser = null)
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                 .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                 .Options;

            var settings = new AppSettings { Secret = "secret" };
            var context = new DataContext(options);


            if (groups != null) context.Groups.AddRange(groups); context.SaveChanges();

            if (user != null)
            {
                if (user is Student) context.Students.Add(user as Student);
                else context.Instructors.Add(user as Instructor);
                context.SaveChanges();
            }
            userID = default;

            if (oldUser != null) userID = context.Users.Add(oldUser).Entity.ID; context.SaveChanges();

            var userService = new UserService(context, settings);
            return userService;
        }

        private class RegisterTestInvalidData : TheoryData<User, RegisterModel, Group[]>
        {
            public RegisterTestInvalidData()
            {
                Add(new User(), new RegisterModel()
                {
                    FirstName = "TESTNAME",
                    LastName = "TESTNAME",
                    Password = "TESTPASSWORD",
                    MobileID = "TESTMOBILE"
                }, null);

                Add(new User(), new RegisterModel()
                {
                    FirstName = "TESTNAME",
                    LastName = "TESTNAME",
                    Password = "TESTPASSWORD",
                    MobileID = "TESTMOBILE",
                    Group = "TESTGROUP"
                }, null);

                Add(new User(), new RegisterModel()
                {
                    FirstName = "TESTNAME",
                    LastName = "TESTNAME",
                    Password = "TESTPASSWORD",
                    MobileID = "TESTMOBILE",
                    Group = "КИ18-13Б",
                }, new Group[] { new Group { ID = "КИ18-18Б" } });
            }
        }

        private class RegisterTestValidData : TheoryData<User, RegisterModel, Group[]>
        {
            public RegisterTestValidData()
            {
                Add(new User(), new RegisterModel()
                {
                    FirstName = "TESTNAME",
                    LastName = "TESTNAME",
                    Password = "TESTPASSWORD",
                    MobileID = "TESTMOBILE",
                    Group = "КИ18-13Б",
                    UserType = MobileAppUser.Student
                }, new Group[] { new Group { ID = "КИ18-13Б" } } );

                Add(new User(), new RegisterModel()
                {
                    FirstName = "TESTNAME",
                    LastName = "TESTNAME",
                    Password = "TESTPASSWORD",
                    MobileID = "TESTMOBILE",
                    UserType = MobileAppUser.Instructor
                }, null);
            }
        }

        private class AuthenticateTestInvalidData : TheoryData<AuthenticateModel, IUniversityMember>
        {
            public AuthenticateTestInvalidData()
            {
                byte[] passwordHash, passwordSalt;
                PasswordHashHandler.CreatePasswordHash("TESTWRONGPASSWORD", out passwordHash, out passwordSalt);
                var user = new User()
                {
                    Email= "TESTUSERNAME",
                    MobileID = "TESTMOBILE",
                    PasswordSalt = passwordSalt,
                    PasswordHash = passwordHash
                };
                Add(new AuthenticateModel()
                {
                    MobileID = "TESTMOBILE",
                    Password = "TESTPASSWORD",
                    Username = "TESTUSERNAME"
                }, new Instructor() { User = user });

                PasswordHashHandler.CreatePasswordHash("TESTPASSWORD", out passwordHash, out passwordSalt);
                user = new User()
                {
                    Email = "TESTUSERNAME",
                    MobileID = "TESTWRONGMOBILE",
                    PasswordSalt = passwordSalt,
                    PasswordHash = passwordHash
                };
                Add(new AuthenticateModel()
                {
                    MobileID = "TESTMOBILE",
                    Password = "TESTPASSWORD",
                    Username = "TESTUSERNAME"
                }, new Instructor() { User = user });

                PasswordHashHandler.CreatePasswordHash("TESTPASSWORD", out passwordHash, out passwordSalt);
                user = new User()
                {
                    Email = "TESTWRONGUSERNAME",
                    MobileID = "TESTWRONGMOBILE",
                    PasswordSalt = passwordSalt,
                    PasswordHash = passwordHash
                };
                Add(new AuthenticateModel()
                {
                    MobileID = "TESTMOBILE",
                    Password = "TESTPASSWORD",
                    Username = "TESTUSERNAME"
                }, new Instructor() { User = user });

                PasswordHashHandler.CreatePasswordHash("TESTWRONGPASSWORD", out passwordHash, out passwordSalt);
                user = new User()
                {
                    Email = "TESTUSERNAME",
                    MobileID = "TESTMOBILE",
                    PasswordSalt = passwordSalt,
                    PasswordHash = passwordHash
                };
                Add(new AuthenticateModel()
                {
                    MobileID = "TESTMOBILE",
                    Password = "TESTPASSWORD",
                    Username = "TESTUSERNAME"
                }, new Student() { User = user });
            }
        }

        private class AuthenticateTestValidData : TheoryData<AuthenticateModel, IUniversityMember>
        {
            public AuthenticateTestValidData()
            {
                byte[] passwordHash, passwordSalt;
                PasswordHashHandler.CreatePasswordHash("TESTPASSWORD", out passwordHash, out passwordSalt);
                var user = new User()
                {
                    Email = "TESTUSERNAME",
                    MobileID = "TESTMOBILE",
                    PasswordSalt = passwordSalt,
                    PasswordHash = passwordHash
                };
                Add(new AuthenticateModel()
                {
                    MobileID = "TESTMOBILE",
                    Password = "TESTPASSWORD",
                    Username = "TESTUSERNAME"
                }, new Student() { User = user });

                PasswordHashHandler.CreatePasswordHash("TESTPASSWORD", out passwordHash, out passwordSalt);
                user = new User()
                {
                    Email = "TESTUSERNAME",
                    MobileID = "TESTMOBILE",
                    PasswordSalt = passwordSalt,
                    PasswordHash = passwordHash
                };
                Add(new AuthenticateModel()
                {
                    MobileID = "TESTMOBILE",
                    Password = "TESTPASSWORD",
                    Username = "TESTUSERNAME"
                }, new Instructor() { User = user });
            }
        }

        private class UpdateTestValidData : TheoryData<User, User, string>
        {
            public UpdateTestValidData()
            {
                // существующий пользователь
                byte[] passwordHash, passwordSalt;
                PasswordHashHandler.CreatePasswordHash("TESTPASSWORD", out passwordHash, out passwordSalt);
                var oldUser = new User()
                {
                    Email = "TESTUSERNAME",
                    MobileID = "TESTMOBILE",
                    PasswordSalt = passwordSalt,
                    PasswordHash = passwordHash
                };

                // пароль который хотят изменить
                string password = null;

                // данные для изменения
                var newUser = new User()
                {
                    Email = "NEWTESTUSERNAME",
                };

                Add(oldUser, newUser, password);
            }
        }

        private class UpdateTestInvalidData : TheoryData<User, User, string>
        {
            public UpdateTestInvalidData()
            {
                // существующий пользователь
                byte[] passwordHash, passwordSalt;
                PasswordHashHandler.CreatePasswordHash("TESTPASSWORD", out passwordHash, out passwordSalt);
                var oldUser = new User()
                {
                    Email = "TESTUSERNAME",
                    MobileID = "TESTMOBILE",
                    PasswordSalt = passwordSalt,
                    PasswordHash = passwordHash
                };

                // пароль который хотят изменить
                string password = null;

                // данные для изменения
                var newUser = new User()
                {
                    Email = "NEWTESTUSERNAME",
                };

                Add(oldUser, newUser, password);
            }
        }
    }
}