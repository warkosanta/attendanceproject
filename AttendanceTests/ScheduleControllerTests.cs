﻿using AttendanceProjectAPI.Controllers;
using AutoMapper;
using System;
using Xunit;
using ScheduleService.Models;
using Microsoft.EntityFrameworkCore;
using WebApi.Helpers;
using EntitiesLibrary.Helpers;
using AttendanceProjectAPI.Helpers;
using System.Linq;

namespace AttendanceTests
{
    /// <summary>
    /// Проверка работоспособности ScheduleController'а.
    /// </summary>
    public class ScheduleControllerTests
    {
        /// <summary>
        /// Проверка, что метод возвращает корректное количество элементов.
        /// </summary>
        /// <param name="scheduleParams">Параметры сортировки.</param>
        /// <param name="count">Количество элементов конечного результата.</param>
        [Theory]
        [ClassData(typeof(ScheduleControllerData))]
        public void ScheduleController_Get_ShouldReturnCorrectValue(ScheduleParams scheduleParams, int count)
        {
            // Arange
            MapperConfiguration config = new MapperConfiguration(cfg =>           
                cfg.AddProfile(new AutoMapperProfile())); 
            
            IMapper mapper = config.CreateMapper();
            var scheduleService = SetUpScheduleService();
            var controller = new ScheduleController(scheduleService, mapper);

            // Act
            var result = controller.Get(scheduleParams).Count();

            // Assert
            Assert.Equal(count, result);
        }

        private ScheduleService.Services.ScheduleService SetUpScheduleService()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                 .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                 .Options;

            var settings = new AppSettings { Secret = "secret" };
            var context = new DataContext(options);
            context.EnsureDatabaseSeeded();

            var scheduleService = new ScheduleService.Services.ScheduleService(context);
            return scheduleService;
        }
    }

    class ScheduleControllerData : TheoryData<ScheduleParams, int>
    {
        public ScheduleControllerData()
        {
            Add(new ScheduleParams(), 10);
        }
    }
}
