﻿using AutoMapper;
using EntitiesLibrary.Interfaces;
using StudentService.Models;
using System.Linq;
using System;
using EntitiesLibrary.Entities;
using AttendanceService.Models;
using System.Collections.Generic;

namespace StudentService.Services
{
    public class AttendanceManager
    {
        private IAppDataContext _context;
        private IMapper _mapper;

        public AttendanceManager(IAppDataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Attend(AttendModel model)
        {
            var student = _context.Students.FirstOrDefault(s => s.UserID == Guid.Parse(model.UserID));
            _ = student ?? throw new NotImplementedException();

            var attendance = _mapper.Map<Attendance>(model);
            attendance.DateTime = DateTime.Now;

            _context.Attendance.Add(attendance);
        }

        /// <summary>
        /// Get all attended students for specific schedule item ID and date.
        /// </summary>
        /// <param name="getParams">Specific schedule item ID and date.</param>
        /// <returns></returns>
        public AttendanceInfoDTO GetAttendedStudents(AttendanceGetParams getParams)
        {
            // if given guid id consist in database
            if (_context.Attendance.Any(a => a.ScheduleID == getParams.ScheduleItemID))
            {
                // found schedule item by given id
                var scheduleItem = _context.Schedule.Where(s => s.ID == getParams.ScheduleItemID).First();

                // students who attende class
                List<int> attendedStudentsID;

                // if valid date is given, return ids of all students who attende class at given date
                if (DateTime.TryParse(getParams.AttendanceDate, out DateTime date))
                {
                    attendedStudentsID = _context.Attendance.Where(a => a.ScheduleID == scheduleItem.ID &&
                        a.DateTime.Date == date.Date).Select(a => a.StudentID).ToList();
                }
                // date is not valid, return ids of all students who attende class for all time
                else
                {
                    attendedStudentsID = _context.Attendance.Where(a => a.ScheduleID == scheduleItem.ID)
                        .Select(a => a.StudentID).ToList();
                }

                // get students by sorted ids
                var students = _context.Students.Where(s => attendedStudentsID.Contains(s.ID)).ToList();
                
                // create and return DTO that contains both students and schedule item info
                AttendanceInfoDTO attendance = new AttendanceInfoDTO
                {
                    ScheduleItem = scheduleItem,
                    AttendedStudents = _mapper.Map<List<StudentDTO>>(students)
                };
                return attendance;
            }
            // given guid id doesnt consist in database
            else
            {
                // TODO: exception filter
                throw new Exception();
            }
        }      
    }
}
