﻿using EntitiesLibrary.Entities;
using System;

namespace AttendanceService.Models
{
    /// <summary>
    /// DTO for Student entity.
    /// </summary>
    public class StudentDTO
    {
        public Guid UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Group Group { get; set; }
    }
}
