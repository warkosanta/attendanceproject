﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StudentService.Models
{
    public class AttendModel: IValidatableObject
    {
        [Required(ErrorMessage = "Correct GUID for schedule item is required.")]
        public Guid ScheduleID { get; set; }

        public string UserID { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!Guid.TryParse(UserID, out Guid userid))
                yield return new ValidationResult("Correct GUID for user is required.");
                
        }
    }
}
