﻿using EntitiesLibrary.Entities;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace AttendanceService.Models
{
    public class AttendanceInfoDTO
    {
        public Schedule ScheduleItem { get; set; }
        
        public List<StudentDTO> AttendedStudents { get; set; }
    }
}
