﻿using EntitiesLibrary.Entities;
using System.Collections.Generic;

namespace AttendanceService.Models
{
    public class StudentAttendanceDTO
    {
        public Student Student { get; set; }
        public List<Attendance> Attendance { get; set; }
    }
}
