﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AttendanceService.Models
{
    /// <summary>
    /// Parameters for Attendance GET method.
    /// </summary>
    public class AttendanceGetParams
    {
        /// <summary>
        /// Schedule Item ID.
        /// </summary>
        /// <remarks>
        /// Supposed to be GUID.
        /// </remarks>
        [Required(ErrorMessage ="Correct GUID for schedule item is required.")]
        public Guid ScheduleItemID { get; set; }

        /// <summary>
        /// User ID.
        /// </summary>
        /// <remarks>
        /// Supposed to be GUID. 
        /// </remarks>
        public Guid UserID { get; set; }

        /// <summary>
        /// Attendance day.
        /// </summary>
        /// <remarks>
        /// Format include dd, mm, yyyy.
        /// </remarks>
        public string AttendanceDate { get; set; }

        /// <summary>
        /// Group ID.
        /// </summary>
        /// <remarks>
        /// Could be in both registers.
        /// </remarks>
        public string Group { get; set; }
    }
}